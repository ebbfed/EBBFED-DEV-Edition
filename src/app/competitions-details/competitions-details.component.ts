import { Component, OnInit, ChangeDetectionStrategy, ViewChild, TemplateRef } from '@angular/core';
import { DatePipe } from '@angular/common'
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { Subject } from 'rxjs';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView } from 'angular-calendar';
import * as jQuery from 'jquery';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-competitions-details',
  templateUrl: './competitions-details.component.html',
  styleUrls: ['./competitions-details.component.css'],
  providers: [DatePipe]

})
export class CompetitionsDetailsComponent implements OnInit {
  Matches: any;
  events: CalendarEvent[];
  refresh: Subject<any> = new Subject();
  activeDayIsOpen: boolean = false;
  view: CalendarView;
  CalendarView: any;
  viewDate: Date = new Date();
  @ViewChild('modalContent')
  modalContent: TemplateRef<any>;
  modalData: {
    action: string;
    event: CalendarEvent;
  };
  Comp: any;

  CompTables: any;

  CurrentURL: any;
  CompScores: any;

  constructor(public http: HttpClient, public Route: ActivatedRoute, private modal: NgbModal, private date: DatePipe, public Routee: Router) {
    this.events = [];

    this.Matches = [{
      "ClubA": "",
      "ClubB": "",
      "Date": "",
      "ScoreA": "",
      "ScoreB": "",
      "LiveLink": "",
      "Ages": "",
      "Place": "",
      "Gender": "",
      "Division": {
        "ID": "",
        "NameA": "",
        "NameE": ""
      },
      "Round": {
        "ID": "",
        "NameA": "",
        "NameE": ""
      },
      "Age": {
        "ID": "",
        "NameA": "",
        "NameE": ""
      },
    }]

    this.view = CalendarView.Month;
    this.CalendarView = CalendarView;

    this.Route.params.subscribe(routeParams => {
      this.GetCompDetails(routeParams.id);
    });

    this.CurrentURL = this.Route.url;
  }

  public CompID = this.Route.snapshot.paramMap.get('id');

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }
  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.refresh.next();
  }

  //change api for commpetition
  GetMatchesMonth() {
    this.http.get(`http://yakensolution.cloudapp.net/EB/api/Competitions/GetAllComptetionMatches?id=${this.CompID}`).subscribe((res: any) => {
      if (res.isSuccess) {
        this.Matches = res.Response.CompetitionsTable;
        console.log(this.Matches);

        let s = 'VS';
        //(this.Matches);
        for (var i = 0; this.Matches !== undefined && i < this.Matches.length; i++) {
          this.events.push({
            start: startOfDay(new Date(this.Matches[i].Date)),
            end: endOfDay(new Date(this.Matches[i].Date)),
            title: `<div class="widget widget_matches">
            <ul>
              <li class="text-center">
                <div class="row">
                  <div class="col-md-4">
                    <div class="text-center">
                      <h6><a>${this.Matches[i].TeamA}<img style="max-height: 50px;" src="http://yakensolution.cloudapp.net/EB/Content/Images/${this.Matches[i].LogoA}"></a></h6>
                    </div>

                  </div>

                  <div class="col-md-4"></h4><h2>VS</h2></div>

                  <div class="col-md-4">
                    <div class="text-center">
                      <h6><a>${this.Matches[i].TeamB}<img style="max-height: 50px;" src="http://yakensolution.cloudapp.net/EB/Content/Images/${this.Matches[i].LogoB}"></a></h6>
                    </div>

                  </div>

                </div>

              </li>

            </ul>
          </div>
`,
            allDay: true,
          });

        }
        this.refresh.next();

      }

    });
  }
  GetCompDetails(CompID) {
    this.http.get(`http://yakensolution.cloudapp.net/EB/api/Competitions/AllCompetitions`).subscribe((res: any) => {
      if (res.isSuccess) {
        this.Comp = res.Response.AllCompetitions.find(c => c.ID == CompID);
        console.log(this.Comp);
      }
    });
  }

  GetCompTables() {
    this.http.get(`http://yakensolution.cloudapp.net/EB/api/Competitions/CompetitionsTable?id=${this.CompID}`).subscribe((res: any) => {
      if (res.isSuccess) {
        this.CompTables = res.Response.CompetitionsTable;
        console.log(this.CompTables);
      }
    });
  }

  GetCompScores() {
    this.http.get(`http://yakensolution.cloudapp.net/EB/api/Competitions/CompetitionScore?id=${this.CompID}`).subscribe((res: any) => {
      if (res.isSuccess) {
        this.CompScores = res.Response.CompetitionScore;
        console.log(this.CompTables);
      }
    });
  }

  ngOnInit() {
    this.GetMatchesMonth();
    this.GetCompTables();
    this.GetCompScores();
  }

}
