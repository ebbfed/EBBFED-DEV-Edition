import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterPlayersComponent } from './inter-players.component';

describe('InterPlayersComponent', () => {
  let component: InterPlayersComponent;
  let fixture: ComponentFixture<InterPlayersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterPlayersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterPlayersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
