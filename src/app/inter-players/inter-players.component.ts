import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-inter-players',
  templateUrl: './inter-players.component.html',
  styleUrls: ['./inter-players.component.css']
})
export class InterPlayersComponent implements OnInit {
  NMatches: any;
  Allplayers: any;
  Clubs: any;
  ClubName: String;
  PlayerName: any;
  FilteredPlayers: any;
  constructor(public http: HttpClient, public Route: ActivatedRoute) {
    this.NMatches = [{
      "ClubA": "",
      "ClubB": "",
      "Date": "",
      "LiveLink": "",
      "Age": "",
      "Place": "",
      "Gender": "",
      "Division": ""
    }]
    this.FilteredPlayers = [];
    this.Clubs = [{
      "ID": "",
      "NameA": "",
      "NameE": "",
      "DescriptionA": "",
      "DescriptionE": "",
      "EstablishedYear": "",
      "NumberTitles": "",
      "Division": "",
      "Logo": ""
    }];

    this.Allplayers = [{
      "ID": "",
      "NameA": "",
      "NameE": "",
      "Age": "",
      "Center": "",
      "Height": "",
      "Weight": "",
      "InternationalMatches": "",
      "PlayerType": "",
      "ClubName": "",
      "ImageUrl": "",
      "TransfarFrom": "",
      "TransfarTo": "",
      "Year": ""
    }];
  }
  NextMatches() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Matches/NextMatche').subscribe((res: any) => {
      if (res.isSuccess) {
        this.NMatches = res.Response.NextMatche;
        console.log(this.NMatches);
      }

    });
  }

  GetAllPlayers() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Players/AllPlayers').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Allplayers = [];
        for (var i = 0; res.Response.Players !== undefined && i < res.Response.Players.length; i++) {
          if (res.Response.Players[i].PlayerType === "InterNational") {
            this.Allplayers.push(res.Response.Players[i]);
          }
        }
        console.log(res.Response.Players);
        this.FilteredPlayers = this.Allplayers;
        console.log(this.Allplayers);
      }

    });
  }
  FilterPlayers(x) {
    console.log(x);
    this.FilteredPlayers = [];
    if (x === '') {
      this.FilteredPlayers = this.Allplayers;
    }
    for (var i = 0; this.Allplayers !== undefined && i < this.Allplayers.length; i++) {

      if (this.Allplayers[i].ClubName === x.NameE || this.Allplayers[i].ClubName === x.NameA) {
        this.FilteredPlayers.push(this.Allplayers[i]);
      }
    }
    console.log(this.FilteredPlayers);
  }
  GetAllClubs() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Clubs/AllClubs').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Clubs = res.Response.Players;
        console.log(this.Clubs);
      }
    });

  }


  errorHandler(event) {
    console.debug(event);
    event.target.src = "assets/images/logo.jpg";
  }

  ngOnInit() {
    this.NextMatches();
    this.GetAllPlayers();
    this.GetAllClubs();
  }
}
