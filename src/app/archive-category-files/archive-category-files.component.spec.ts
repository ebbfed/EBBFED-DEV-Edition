import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchiveCategoryFilesComponent } from './archive-category-files.component';

describe('ArchiveCategoryFilesComponent', () => {
  let component: ArchiveCategoryFilesComponent;
  let fixture: ComponentFixture<ArchiveCategoryFilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchiveCategoryFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchiveCategoryFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
