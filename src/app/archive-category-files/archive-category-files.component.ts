import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-archive-category-files',
  templateUrl: './archive-category-files.component.html',
  styleUrls: ['./archive-category-files.component.css']
})
export class ArchiveCategoryFilesComponent implements OnInit {

  Arch : any;

  CurrentURL: any;

  constructor(public http: HttpClient, public Route: ActivatedRoute) {
    this.Arch = {
      "ArchiveCategory": [{
        "ID": "",
        "NameA": "",
        "NameE": ""
      }],
      "Files": [{
        "ID": "",
        "NameA": "",
        "NameE": "",
        "Path": ""
      }]
    };

    this.Route.params.subscribe(routeParams => {
      this.GetArchDetails(routeParams.id);
    });

    this.CurrentURL = this.Route.url;
  }

  public ArchID = this.Route.snapshot.paramMap.get('id');

  GetArchDetails(ArchID) {
    this.http.get(`http://yakensolution.cloudapp.net/EB/api/Archive/ArchiveCategoryFiles?id=${ArchID}`).subscribe((res: any) => {
      if (res.isSuccess) {
        this.Arch = res.Response;
        console.log(this.Arch);
      }
    });
  }

  ngOnInit() {
  }

}
