import { Component, OnInit, ChangeDetectionStrategy, ViewChild, TemplateRef } from '@angular/core';
import { DatePipe } from '@angular/common'
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

import { Subject } from 'rxjs';

import * as jQuery from 'jquery';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';

@Component({
  selector: 'app-table-matches',
  templateUrl: './table-matches.component.html',
  styleUrls: ['./table-matches.component.css'],
  providers: [DatePipe]
})
export class TableMatchesComponent implements OnInit {
  Table: any;
  NMatches: any;
  Matches: any;
  FilteredMatches: any;
  FilteredTables: any;
  ComptetionMatches:any;
  Age: any; Div: any;
  Clubs: any;
  Ages: any;
  Divs: any;
  Rules : any ;
  @ViewChild('modalContent')
  modalContent: TemplateRef<any>;
  exportAsConfig: ExportAsConfig = {
    type: 'pdf', // the type you want to download
    elementId: 'matchTable' , // the id of html/table element
  }


  constructor(public http: HttpClient, public Route: ActivatedRoute, private modal: NgbModal, private date: DatePipe, public Routee: Router
    ,private exportAsService: ExportAsService) {
    this.ComptetionMatches = [{
      "ID": 25,
      "TeamA": "",
      "TeamB": "",
      "ScoreA": 0,
      "ScoreB": 0,
      "Date": "",
      "LogoA": "",
      "LogoB": "",
      "Time": "",
      "Location": "",
      "CompetitionNameA": "",
      "CompetitionNameE": ""
    }]
    this.Table = [
      {
        "ID": "",
        "ClubA": {
          "ID" : "",
          "NameA" : "",
          "Logo" : ""
        },
        "ClubB": {
          "ID" : "",
          "NameA" : "",
          "Logo" : ""
        },
        "NumberOfMatches": "",
        "Winner": "",
        "Lose": "",
        "Points": "",
        "INGoals": "",
        "OutGoals": "",
        "DivisionID": "",
        "AgeID": "",
        "Gender": "",
        "RoundID": "",
        "Division": {
          "ID" : "",
          "NameA" : "",
          "NameE" : ""
        },
        "Round": {
          "ID" : "",
          "NameA" : "",
          "NameE" : ""
        },
        "Age": {
          "ID" : "",
          "NameA" : "",
          "NameE" : ""
        },
      }];
    this.NMatches = [{
      "ClubA": "",
      "ClubB": "",
      "Date": "",
      "LiveLink": "",
      "Age": "",
      "Place": "",
      "Gender": "",
      "Division": ""
    }];
    this.Clubs = [{
      "ID": "",
      "NameA": "",
      "NameE": "",
      "DescriptionA": "",
      "DescriptionE": "",
      "EstablishedYear": "",
      "NumberTitles": "",
      "Division": "",
      "Logo": ""
    }];
    this.FilteredMatches = [];
    this.FilteredTables = [];
    this.Rules = [{

      "NameA": "",
      "NameE": "",
      "PdfName": ""
    }]
    this.Matches = [{
      "ClubA": "",
      "ClubB": "",
      "Date": "",
      "ScoreA": "",
      "ScoreB": "",
      "LiveLink": "",
      "Ages": "",
      "Place": "",
      "Gender": "",
      "Division": {
        "ID" : "",
        "NameA" : "",
        "NameE" : ""
      },
      "Round": {
        "ID" : "",
        "NameA" : "",
        "NameE" : ""
      },
      "Age": {
        "ID" : "",
        "NameA" : "",
        "NameE" : ""
      },
    }]



    this.Ages = [{
      "ID": "",
      "NameA": "",
      "NameE": ""
    }];
    this.Divs = [{
      "ID": "",
      "NameA": "",
      "NameE": ""
    }];

  }

  NextMatches() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Matches/NextMatche').subscribe((res: any) => {
      if (res.isSuccess) {
        this.NMatches = res.Response.NextMatche;
        //(this.NMatches);
      }

    });
  }
  GetAllClubs() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Clubs/AllClubs').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Clubs = res.Response.Players;
        //(this.Clubs);
      }
    });

  }
  GetNLeagueTable() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/NationalMatches/TableMatches').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Table = res.Response.TableMatches;
        //(this.Table);
      }

    });

  }
  GetAges() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Settings/AllAges').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Ages = res.Response.Ages;
        //(this.Ages);
      }

    });
  }
  GetDivs() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Settings/AllDivisions').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Divs = res.Response.Divisions;
        //(this.Divs);
      }

    });
  }
  GetRules(){
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Rules/AllRules').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Rules = res.Response.Rules;
        console.log(this.Rules);
      }

    });

  }
  GetAllComptetionMatches() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Competitions/GetAllComptetionMatches?id=-1&ageId=-1').subscribe((res: any) => {
      if (res.isSuccess) {
        this.ComptetionMatches = res.Response.CompetitionsTable;
        console.log(this.ComptetionMatches);
      }
    });
  }
  Filters(data){
    let filter = 'id=-1';
    if (data.ageId !== '') {
      filter += ('&ageId=' + data.ageId);
    } else {
      filter += ('&ageId=-1');
    }
    if (data.gender !== '') {
      filter += ('&gender=' + data.gender);
    }
    console.log(data);
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Competitions/GetAllComptetionMatches?' + filter).subscribe((res: any) => {
      this.ComptetionMatches = res.Response.CompetitionsTable;
    });
  }


export() {
  this.exportAsService.save(this.exportAsConfig, 'Table Competitions').subscribe(() => {
  });
}
  ngOnInit() {
    if (this.Routee.url === '/matches-table') {
      this.NextMatches();
      this.GetAges();
      this.GetDivs();

    }
    else {
      this.NextMatches();
      this.GetAges();
      this.GetDivs();

    }
    this.GetRules();
    this.GetAllComptetionMatches();

  }

}
