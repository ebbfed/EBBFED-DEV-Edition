import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ShareButtonsModule } from '@ngx-share/buttons';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NewsComponent } from './news/news.component';
import { NewsDetailsComponent } from './news-details/news-details.component';
import { TableMatchesComponent } from './table-matches/table-matches.component';
import { PlayersComponent } from './players/players.component';
import { PlayerDetailsComponent } from './player-details/player-details.component';
import { RRComponent } from './rr/rr.component';
import { MatchResultsComponent } from './match-results/match-results.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { UnionHistoryComponent } from './union-history/union-history.component';
import { UnionDecsComponent } from './union-decs/union-decs.component';
import { UnionDecDetailsComponent } from './union-dec-details/union-dec-details.component';
import { UnionPresidentsComponent } from './union-presidents/union-presidents.component';
import { SearchfilterPipe } from './searchfilter.pipe';
import { ClubComponent } from './club/club.component';
import { ClubDetailsComponent } from './club-details/club-details.component';
import { RulersComponent } from './rulers/rulers.component';
import { CoachesComponent } from './coaches/coaches.component';
import { CoachesDetailsComponent } from './coaches-details/coaches-details.component';
import { ArticlesComponent } from './articles/articles.component';
import { ArticlesDetailsComponent } from './articles-details/articles-details.component';
import { CoursesComponent } from './courses/courses.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { GalleryComponent } from './gallery/gallery.component';
import { LeagueRankingComponent } from './league-ranking/league-ranking.component';
import { InterPlayersComponent } from './inter-players/inter-players.component';
import { UnionCommitteesComponent } from './union-committees/union-committees.component';
import { UnionCommitteesDetailsComponent } from './union-committees-details/union-committees-details.component';
import { CompetitionsComponent } from './competitions/competitions.component';
import { CompetitionsDetailsComponent } from './competitions-details/competitions-details.component';
import { ArchiveComponent } from './archive/archive.component';
import { ArchiveCategoryFilesComponent } from './archive-category-files/archive-category-files.component';
import { StatisticalComponent } from './statistical/statistical.component';
import { BranchesComponent } from './branches/branches.component';
import { HistoryChampionshipsComponent } from './history-championships/history-championships.component';
import { BranchDetailsComponent } from './branch-details/branch-details.component';

import { ExportAsModule } from 'ngx-export-as';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'news', component: NewsComponent },
  { path: 'gallery', component: GalleryComponent },
  { path: 'courses', component: CoursesComponent },
  { path: 'course-details/:id', component: CourseDetailsComponent },
  { path: 'articles', component: ArticlesComponent },
  { path: 'national-news', component: NewsComponent },
  { path: 'news-details/:id', component: NewsDetailsComponent },
  { path: 'articles-details/:id', component: ArticlesDetailsComponent },
  { path: 'news-details/:id', component: NewsDetailsComponent },

  { path: 'matches-table', component: TableMatchesComponent },
  { path: 'match-result', component: MatchResultsComponent },
  { path: 'national-matches-table', component: TableMatchesComponent },
  { path: 'national-match-result', component: MatchResultsComponent },
  { path: 'competitions', component: CompetitionsComponent },
  { path: 'competitions-details/:id', component: CompetitionsDetailsComponent },


  { path: 'rules&regulations', component: RRComponent },
  { path: 'players', component: PlayersComponent },
  { path: 'clubs', component: ClubComponent },
  { path: 'club-details/:id', component: ClubDetailsComponent },
  { path: 'branches', component: BranchesComponent },
  { path: 'branch-details/:id', component: BranchDetailsComponent },
  { path: 'coaches', component: CoachesComponent },
  { path: 'coache-details/:id', component: CoachesDetailsComponent },
  { path: 'rulers', component: RulersComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'player-details/:id', component: PlayerDetailsComponent },
  { path: 'union-history', component: UnionHistoryComponent },
  { path: 'union-decisions', component: UnionDecsComponent },
  { path: 'union-decision-details/:id', component: UnionDecDetailsComponent },
  { path: 'union-committees', component: UnionCommitteesComponent },
  { path: 'union-committees-details/:id', component: UnionCommitteesDetailsComponent },
  { path: 'statistical', component: StatisticalComponent },

  { path: 'union-presidents', component: UnionPresidentsComponent },
  { path: 'league-ranking', component: LeagueRankingComponent },
  { path: 'inter-players', component: InterPlayersComponent },
  { path: 'archive', component: ArchiveComponent },
  { path: 'history-championships', component: HistoryChampionshipsComponent },
  { path: 'archive-category-files/:id', component: ArchiveCategoryFilesComponent },
];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NewsComponent,
    NewsDetailsComponent,
    TableMatchesComponent,
    PlayersComponent,
    PlayerDetailsComponent,
    RRComponent,
    MatchResultsComponent,
    ContactUsComponent,
    UnionHistoryComponent,
    UnionDecsComponent,
    UnionDecDetailsComponent,
    UnionPresidentsComponent,
    SearchfilterPipe,
    ClubComponent,
    ClubDetailsComponent,
    RulersComponent,
    CoachesComponent,
    CoachesDetailsComponent,
    ArticlesComponent,
    ArticlesDetailsComponent,
    CoursesComponent,
    CourseDetailsComponent,
    GalleryComponent,
    LeagueRankingComponent,
    InterPlayersComponent,
    UnionCommitteesComponent,
    UnionCommitteesDetailsComponent,
    CompetitionsComponent,
    CompetitionsDetailsComponent,
    ArchiveComponent,
    ArchiveCategoryFilesComponent,
    StatisticalComponent,
    BranchesComponent,
    HistoryChampionshipsComponent,
    BranchDetailsComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientJsonpModule,
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    FilterPipeModule,
    RouterModule.forRoot(
      appRoutes,
      { useHash: true, enableTracing: false, scrollPositionRestoration: 'enabled' }),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    NgbModule,
    
    ShareButtonsModule.forRoot(),
    ExportAsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
