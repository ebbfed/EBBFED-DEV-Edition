import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryChampionshipsComponent } from './history-championships.component';

describe('HistoryChampionshipsComponent', () => {
  let component: HistoryChampionshipsComponent;
  let fixture: ComponentFixture<HistoryChampionshipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryChampionshipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryChampionshipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
