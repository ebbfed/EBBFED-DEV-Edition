import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-history-championships',
  templateUrl: './history-championships.component.html',
  styleUrls: ['./history-championships.component.css']
})
export class HistoryChampionshipsComponent implements OnInit {
  SeasonHistory: any;
  Ages: any;
  Seasons: any;

  constructor(private http: HttpClient, public Route: Router) {
    this.SeasonHistory = [{
     "Season": "2018 - 2019",
      "Teams": [{
        "ID": 0,
        "Rank": 0,
        "Season": "",
        "TeamNameAr": "",
        "TeamNameEn": "",
        "AgeAr": "",
        "AgeEn": "",
        "Gender": ""
        }]
    }];
    this.Ages = [{
      "ID": "",
      "NameA": "",
      "NameE": ""
    }];
    this.Seasons = [{
      "ID": 0,
      "Season": ""
    }];
  }
  GetSeasonHistory() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Seasons/GetSeasonHistory?').subscribe((res: any) => {
      if (res.isSuccess) {
        this.SeasonHistory = res.Response.SeasonTable;
      }
    })
  }
  GetAges() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Settings/AllAges').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Ages = res.Response.Ages;
      }
    });
  }
  GetSeasons() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Seasons/GetSeasons').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Seasons = res.Response.Seasons;
      }
    });
  }
  Filters(data) {
    let filter = '';
    console.log(data);
    if (data.seasonId !== '') {
      filter += ('seasonId=' + data.seasonId);
    } else {
      filter += ('seasonId=-1');
    }
    if (data.ageId !== '') {
      filter += ('&ageId=' + data.ageId);
    } else {
      filter += ('&ageId=-1');
    }
    if (data.gender !== '') {
      filter += ('&gender=' + data.gender);
    }
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Seasons/GetSeasonHistory?' + filter).subscribe((res:any)=>{
      if (res.isSuccess) {
        this.SeasonHistory = res.Response.SeasonTable;
      }
    });

  }
  ngOnInit() {
    this.GetAges();
    this.GetSeasons();
    this.GetSeasonHistory();
  }

}
