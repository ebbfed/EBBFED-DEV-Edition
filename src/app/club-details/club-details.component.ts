import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-club-details',
  templateUrl: './club-details.component.html',
  styleUrls: ['./club-details.component.css']
})
export class ClubDetailsComponent implements OnInit {
  Club :   {
    "ID": "",
    "NameA": "",
    "NameE": "",
    "DescriptionA": "",
    "DescriptionE": "",
    "EstablishedYear": "",
    "TitlesWon": "",
    "Division": "",
    "Logo": "",
    "Area" : {
      "ID" : "",
      "NameA" : "",
      "NameE" : ""
    },
  } ;
  NMatches :any ;

  CurrentURL : any;

  ClubGallery : any;

  Allplayers : any;

  CPlayers : any;

  constructor(public http: HttpClient, public Route: ActivatedRoute){
    this.Club = {
      "ID": "",
      "NameA": "",
      "NameE": "",
      "DescriptionA": "",
      "DescriptionE": "",
      "EstablishedYear": "",
      "TitlesWon": "",
      "Division": "",
      "Logo": "",
      "Area" : {
        "ID" : "",
        "NameA" : "",
        "NameE" : ""
      },
    }
    this.NMatches= [{
      "ClubA": "",
      "ClubB": "",
      "Date": "",
      "LiveLink": "",
      "Age": "",
      "Place": "",
      "Gender": "",
      "Division": ""
    }] ;

    this.ClubGallery = [{
      "ID": "",
      "ImageUrl": "",
      "VideoUrl": "",
      "DescriptionA": "",
      "DescriptionE": "",
      "Date" : ""
    }] ;

    this.Allplayers = [{
      "ID": "",
      "NameA": "",
      "NameE": "",
      "Age": "",
      "Center": "",
      "Height": "",
      "Weight": "",
      "InternationalMatches": "",
      "PlayerType": "",
      "ClubName": "",
      "ImageUrl": "",
      "TransfarFrom": "",
      "TransfarTo": "",
      "Year": ""
    }] ;

    this.CPlayers = [];

   this.CurrentURL = this.Route.url;
  }
  public ClubID = this.Route.snapshot.paramMap.get('id');

  GetClubDetails() {
    this.http.get(`http://yakensolution.cloudapp.net/EB/api/Clubs/GetClub?id=${this.ClubID}`).subscribe((res: any) => {
      if (res.isSuccess) {
        this.Club = res.Response.Players[0];
        console.log(this.Club);
      }
    });
  }
  NextMatches() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Matches/NextMatche').subscribe((res: any) => {
      if (res.isSuccess) {
        this.NMatches = res.Response.NextMatche;
        console.log(this.NMatches);
      }

    });
  }
  GetClubGallery(){
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Clubs/FilteredAllGallery?ClubID=' + this.ClubID + '&PlayerID=0').subscribe((res: any) => {
      if (res.isSuccess) {
        this.ClubGallery = res.Response.Gallery ;
        console.log(this.ClubGallery ) ;
      }
    });
  }
  GetCPlayers() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Players/AllPlayers').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Allplayers = res.Response.Players;
        this.CPlayers = [];
        for (var i = 0; res.Response.Players !== undefined && i < res.Response.Players.length; i++) {
          if (res.Response.Players[i].ClubName === this.Club.NameA) {
            this.CPlayers.push(res.Response.Players[i]);
          }
        }
        console.log(this.CPlayers);
      }
    });
  }
  errorHandler(event) {
    console.debug(event);
    event.target.src = "assets/images/logo.jpg";
  }
  ngOnInit() {
    this.GetClubDetails() ;
    this.NextMatches() ;
    this.GetClubGallery();
    this.GetCPlayers();
  }

}
