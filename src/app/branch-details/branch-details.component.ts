import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-branch-details',
  templateUrl: './branch-details.component.html',
  styleUrls: ['./branch-details.component.css']
})
export class BranchDetailsComponent implements OnInit {
  CurrentURL : any;
  public BranchID = this.Route.snapshot.paramMap.get('id');
  branch:any

  constructor(public http: HttpClient, public Route: ActivatedRoute) {
    this.branch  = {
      "ID": 0,
      "NameA": "",
      "NameE": "",
      "AddressA": "",
      "AddressE": "",
      "Phone1": "",
      "Phone2": null,
      "Email": "",
      "Fax": "",
      "Directors": [
        {
          "ID": 0,
          "ImageUrl": "",
          "NameA": "",
          "NameE": "",
          "TitleA": "",
          "TitleE": "",
          "BranchId": 0,
          "BriefA": "",
          "BriefE": ""
        }
      ]
    }
  }
  GetBranch(){
    console.log('http://yakensolution.cloudapp.net/EB/api/Branches/GetBranch?id=' + this.BranchID);
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Branches/GetBranch?id=' + this.BranchID).subscribe((res: any) => {
      if (res.isSuccess) {
        this.branch = res.Response.Branches;
        console.log(this.branch);
      }

    });
  }
  ngOnInit() {
    this.GetBranch();
  }

}
