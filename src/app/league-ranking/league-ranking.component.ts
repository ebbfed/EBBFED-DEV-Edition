import { Component, OnInit, ChangeDetectionStrategy, ViewChild, TemplateRef } from '@angular/core';
import { DatePipe } from '@angular/common'
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { Subject } from 'rxjs';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView } from 'angular-calendar';
import * as jQuery from 'jquery';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-league-ranking',
  templateUrl: './league-ranking.component.html',
  styleUrls: ['./league-ranking.component.css'],
  providers: [DatePipe]
})
export class LeagueRankingComponent implements OnInit {

  Table: any;
  FilteredTables: any;
  FilteredMatches: any;
  Matches: any;
  Ages: any;
  Divs: any;

  Age: any; Div: any; Round: any;

  constructor(public http: HttpClient, public Route: ActivatedRoute, private modal: NgbModal, private date: DatePipe, public Routee: Router) {

  }

  GetLeagueTable() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Matches/TableMatches').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Table = res.Response.TableMatches;
      this.FilterTables(4, 1);

        this.FilteredTables = this.Table;
        //(this.Table);
      }

    })
  }

  GetAges() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Settings/AllAges').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Ages = res.Response.Ages;
        //(this.Ages);
      }

    });
  }
  GetDivs() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Settings/AllDivisions').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Divs = res.Response.Divisions;
        //(this.Divs);
      }

    });
  }

  FilterMatches(x, y) {
    this.FilteredMatches = [];
    //(x);
    //(y);

    if ((x == undefined || x == '') && (y == undefined || y == '')) {
      this.FilteredMatches = this.Matches;
    }
    else if ((x != undefined && x != '') && (y == undefined || y == '')) {
      //('aa');

      for (var i = 0; this.Matches != undefined && i < this.Matches.length; i++) {
        if (this.Matches[i].AgeID == x) {
          this.FilteredMatches.push(this.Matches[i]);
        }
      }

    }
    else if ((x == undefined || x == '') && (y != undefined || y != '')) {
      for (var i = 0; this.Matches != undefined && i < this.Matches.length; i++) {
        if (this.Matches[i].Division.ID === y)
          this.FilteredMatches.push(this.Matches[i]);
      }

    }
    else {
      for (var i = 0; this.Matches != undefined && i < this.Matches.length; i++) {
        if (this.Matches[i].Division.ID === y && this.Matches[i].AgeID === x)
          this.FilteredMatches.push(this.Matches[i]);
      }
    }


    this.FilterTables(x,y);
  }

  FilterTables(x, y) {
    this.FilteredTables = [];
    //(x);
    //(y);

    if ((x == undefined || x == '') && (y == undefined || y == '')) {
      this.FilteredTables = this.Table;
      this.FilterTables(4, 1);
    }
    else if ((x != undefined && x != '') && (y == undefined || y == '')) {
      //('aa');

      for (var i = 0; this.Table != undefined && i < this.Table.length; i++) {
        if (this.Table[i].AgeID == x) {
          this.FilteredTables.push(this.Table[i]);
        }
      }

    }
    else if ((x == undefined || x == '') && (y != undefined || y != '')) {
      for (var i = 0; this.Table != undefined && i < this.Table.length; i++) {
        if (this.Table[i].Division.ID === y)
          this.FilteredTables.push(this.Table[i]);
      }

    }
    else {
      for (var i = 0; this.Table != undefined && i < this.Table.length; i++) {
        if (this.Table[i].Division.ID === y && this.Table[i].AgeID === x)
          this.FilteredTables.push(this.Table[i]);
      }
    }
  }
  Filters(data){
    console.log(data);
    let filter = '';
    if(data.Gender!=''){
      filter+=('Gender=' + data.Gender);
    } else {
      filter+=('Gender=male');
    }
    if(data.AgeID!=""){
      filter+=('&AgeID='+data.AgeID);
    }
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Matches/FilteredTableMatches?'+filter+'&RoundID=0&DivisionID=0').subscribe((res: any) => {
      if (res.isSuccess) {
        this.FilteredTables = res.Response.TableMatches;
        //(this.Divs);
      }

    });
  }

  ngOnInit() {
    this.GetLeagueTable();
    this.GetAges();
    this.GetDivs();

    this.FilterTables(4, 1);
  }

}
