import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  ContactUs: any;
  url: string = "https://maps.google.com/maps?q=cairo&t=&z=13&ie=UTF8&iwloc=&output=embed";
  urlSafe: SafeResourceUrl;

  ContactName: String;
  ContactEmail: String;
  ContactMessage: String;

  constructor(public router: Router, private http: HttpClient, private sanitizer: DomSanitizer) {
    this.ContactUs = {
      "PhoneNumber": "",
      "Address": "",
      "Email": "",
      "LocationLong": "",
      "LocationLat": "",
      "WorkingHours": "",
      "FacebookURL": "",
      "TwitterURL": "",
      "InstagramURL": "",
      "YoutubeURL": "",
    }
  }

  GetContactUs() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/ContactUs/ContactUs').subscribe((res: any) => {
      if (res.isSuccess) {
        this.ContactUs = res.Response.ContactUs[0];
        console.log(this.ContactUs);
      }

    });
  }

  SendMessage(ContactName, ContactEmail, ContactMessage) {
    console.log(JSON.stringify(ContactName) + ", " + JSON.stringify(ContactEmail) + ", " + JSON.stringify(ContactMessage));

    if (!((ContactName == undefined || ContactName == '') || (ContactName == undefined || ContactName == '') || (ContactMessage == undefined || ContactMessage == ''))) {
      this.http.post('http://yakensolution.cloudapp.net/EB/api/ContactUs/SubmitContactUs', { Name: ContactName, Email: ContactEmail, Message: ContactMessage }).subscribe((res: any) => {
        if (res.isSuccess) {
          console.log(res.Response);
        }
      });
    }
  }

  ngOnInit() {
    this.GetContactUs();
    //this.url = "https://maps.google.com/maps?q=" + this.ContactUs.LocationLat + "," + this.ContactUs.LocationLong + "&t=&z=13&ie=UTF8&iwloc=&output=embed";
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
  }

}
