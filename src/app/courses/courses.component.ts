import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {
  Courses: any;
  CourseType : String;
  CourseName : String;
  FilteredCourses : any;
  constructor(private http: HttpClient) {
    this.Courses = [{
      "ID": "",
      "TitleA": "",
      "TitleE": "",
      "ShortDescriptionA": "",
      "ShortDescriptionE": "",
      "LongDescriptionA": "",
      "LongDescriptionE": "",
      "Location": "",
      "Date" : "",
      "CourseType" : ""
    }];
    this.FilteredCourses = [];
  }
  GetAllCourses(){
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Courses/AllCourses').subscribe((res:any)=>{
      if(res.isSuccess){
        this.Courses = res.Response.Courses;
        this.FilteredCourses = this.Courses;
        console.log(this.Courses) ;
      }
    }) ;
  }
  FilterCourses(CourseType){
    console.log(CourseType) ;
    this.FilteredCourses = [] ;
    if(CourseType===''){
      this.FilteredCourses = this.Courses;
    }
    for(var i =0 ; this.Courses !== undefined &&  i<this.Courses.length ; i++) {
        if(this.Courses[i].CourseType === CourseType){
          this.FilteredCourses.push(this.Courses[i]) ;
        }
    }
    console.log(this.FilteredCourses) ;
  }
  errorHandler(event) {
    console.debug(event);
    event.target.src = "assets/images/logo.jpg";
  }
  ngOnInit() {
    this.GetAllCourses() ;
  }

}
