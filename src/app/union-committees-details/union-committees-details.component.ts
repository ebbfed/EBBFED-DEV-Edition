import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-union-committees-details',
  templateUrl: './union-committees-details.component.html',
  styleUrls: ['./union-committees-details.component.css']
})
export class UnionCommitteesDetailsComponent implements OnInit {
  Comm : any;
  CurrentURL: any;
  constructor(public http: HttpClient, public Route: ActivatedRoute) {
    this.Comm = {
      "ObjectivesA": "",
      "ObjectivesE": "",
      "CommitteMember": [{
        "ID": "",
        "NameA": "",
        "NameE": ""
      }]
    };

    this.Route.params.subscribe(routeParams => {
      this.GetCommDetails(routeParams.id);
    });

    this.CurrentURL = this.Route.url;
  }

  public CommID = this.Route.snapshot.paramMap.get('id');

  GetCommDetails(CommID) {
    this.http.get(`http://yakensolution.cloudapp.net/EB/api/Union/GetUnoinCommitteDetails?id=${CommID}`).subscribe((res: any) => {
      if (res.isSuccess) {
        this.Comm = res.Response;
        console.log(this.Comm);
      }
    });
  }

  ngOnInit() {
  }

}
