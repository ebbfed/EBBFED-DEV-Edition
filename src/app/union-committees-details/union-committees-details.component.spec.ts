import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnionCommitteesDetailsComponent } from './union-committees-details.component';

describe('UnionCommitteesDetailsComponent', () => {
  let component: UnionCommitteesDetailsComponent;
  let fixture: ComponentFixture<UnionCommitteesDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnionCommitteesDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnionCommitteesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
