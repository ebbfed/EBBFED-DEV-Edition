import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnionCommitteesComponent } from './union-committees.component';

describe('UnionCommitteesComponent', () => {
  let component: UnionCommitteesComponent;
  let fixture: ComponentFixture<UnionCommitteesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnionCommitteesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnionCommitteesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
