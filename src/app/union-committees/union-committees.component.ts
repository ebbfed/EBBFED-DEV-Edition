import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-union-committees',
  templateUrl: './union-committees.component.html',
  styleUrls: ['./union-committees.component.css']
})
export class UnionCommitteesComponent implements OnInit {

  AllComms : any ;

  constructor(public http: HttpClient, public Route: ActivatedRoute) {
    this.AllComms = [{
      "ID": "",
      "ImageUrl": "",
      "NameA": "",
      "NameE": ""
    }] ;
  }
  GetAllComms() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Union/UnoinCommitte').subscribe((res: any) => {
      if (res.isSuccess) {
        this.AllComms = res.Response.UnoinCommittes;
        console.log(this.AllComms);
      }

    });
  }
  ngOnInit() {
    this.GetAllComms();
  }

}
