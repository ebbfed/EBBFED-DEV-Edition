import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-branches',
  templateUrl: './branches.component.html',
  styleUrls: ['./branches.component.css']
})
export class BranchesComponent implements OnInit {
  Branches: any;
  BranchName: any;

  constructor(private http: HttpClient) {
    this.Branches = [{
      "ID": "",
      "NameA": "",
      "NameE": "",
      "AddressA": "",
      "AddressE": "",
      "Phone1": "",
      "Phone2": "",
      "Email": "",
      "Fax": ""
    }];
  }

  GetAllBranches() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Branches/AllBranches').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Branches = res.Response.Branches;
      }
    });

  }
  errorHandler(event) {
    console.debug(event);
    event.target.src = "assets/images/logo.jpg";
  }
  ngOnInit() {
    this.GetAllBranches();
  }

}
