import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute  , Router} from '@angular/router';

@Component({
  selector: 'app-statistical',
  templateUrl: './statistical.component.html',
  styleUrls: ['./statistical.component.css']
})
export class StatisticalComponent implements OnInit {
  statistical : any ;
  FilteredStatistical : any ;
  statisticalName : any;
  Clubs : any ;
  ClubName : String ;

  constructor(private http : HttpClient) {
    this.statistical = [{
      "ID": "",
      "NameA": "",
      "NameE": "",
      "Clubs": [{
        "ID": 30,
        "NameA": "",
        "NameE": "",
      }],
      "Branches": [{
        "ID": 1,
        "NameA": "تيسيت",
        "NameE": "Test",
        "Phone1": "01006213292",
        "Phone2": null,
        "Fax": "012142564",
        "Email": "i@i.com",
        "AddressA": "شبرا",
        "AddressE": "Shubra"
      }],

    }] ;
    this.FilteredStatistical = [{
      "ID": "",
      "NameA": "",
      "NameE": "",
      "Clubs": [{
        "ID": 30,
        "NameA": "",
        "NameE": "",
      }],
      "Branches": [{
        "ID": 1,
        "NameA": "تيسيت",
        "NameE": "Test",
        "Phone1": "01006213292",
        "Phone2": null,
        "Fax": "012142564",
        "Email": "i@i.com",
        "AddressA": "شبرا",
        "AddressE": "Shubra"
      }],

    }] ;
    this.Clubs = [{
      "ID": "",
      "NameA": "",
      "NameE": "",
      "DescriptionA": "",
      "DescriptionE": "",
      "EstablishedYear": "",
      "NumberTitles": "",
      "Division": "",
      "Logo": ""
    }] ;
  }
  GetAllStatistical(){
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Statisticians/AllStatisticians').subscribe((res:any)=>{
      if(res.isSuccess){
        this.statistical = res.Response.Statisticians ;
        this.FilteredStatistical  = this.statistical ;
        console.log(this.statistical) ;
      }
    }) ;
  }
  GetAllClubs(){
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Clubs/AllClubs').subscribe((res: any) => {
    if (res.isSuccess) {
          this.Clubs = res.Response.Players ;
          console.log(this.Clubs) ;
      }
    });

  }
  FilterStatistical(x :any){
    console.log(x) ;
    this.FilteredStatistical = [] ;
    if(x===''){
      this.FilteredStatistical = this.statistical;
    }
    for(var i =0 ; this.statistical !== undefined &&  i<this.statistical.length ; i++){
        if(this.statistical[i].Clubs[0].NameA ==x){
          this.FilteredStatistical.push(this.statistical[i]) ;
          break ;
        }
    }
    console.log(this.FilteredStatistical) ;
  }
  errorHandler(event) {
    console.debug(event);
    event.target.src = "assets/images/logo.jpg";
  }
  ngOnInit() {
    this.GetAllStatistical() ;
    this.GetAllClubs() ;
  }

}
