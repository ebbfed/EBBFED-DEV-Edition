import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute  , Router} from '@angular/router';
import * as jQuery from 'jquery';
declare const jQuery: jQuery;
import { from } from 'rxjs';
import * as slick from 'slick-carousel';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  LastNews : any ;
  LastScore : any ;
  News: any;
  TableAMales: any;
  TableAFemales: any;
  FPlayers : any ;
  Gallery : any ;
  matchScore:any;
  headerImage: any;
  ImportantNews: any;

  constructor(private http : HttpClient) {
    this.headerImage = {
     'ImageUrl': ''
    };
    this.ImportantNews = [{
      "ID": 29,
      "TitleA": "",
      "TitleE": " ",
      "ShortDescriptionA": "",
      "ShortDescriptionE": "",
      "ImageUrl": "",
      "CreationDate": "",
      "ModifiedDate": "",
      "Views": 0,
    }];
    this.LastNews =
    [{
      "ID": "",
      "TitleA": "",
      "TitleE": "",
      "ShortDescriptionA": "",
      "ShortDescriptionE": "",
      "LongDescriptionA": "",
      "LongDescriptionE": "",
      "ImageUrl": "",
      "CreationDate": "",
      "ModifiedDate": "",
      "Views": "",
      "ShareLink": "",
      "Clubs": []
    } ,
     {
      "ID": "",
      "TitleA": "",
      "TitleE": "",
      "ShortDescriptionA": "",
      "ShortDescriptionE": "",
      "LongDescriptionA": "",
      "LongDescriptionE": "",
      "ImageUrl": "",
      "CreationDate": "",
      "ModifiedDate": "",
      "Views": "",
      "ShareLink": "",
      "Clubs": []
    }
    ,

    {
      "ID": "",
      "TitleA": "",
      "TitleE": "",
      "ShortDescriptionA": "",
      "ShortDescriptionE": "",
      "LongDescriptionA": "",
      "LongDescriptionE": "",
      "ImageUrl": "",
      "CreationDate": "",
      "ModifiedDate": "",
      "Views": "",
      "ShareLink": "",
      "Clubs": []
    } ]
    this.LastScore = {
      "ID": "",
      "ClubA": "",
      "ClubB": "",
      "Division": "",
      "Gender": "",
      "Date": "",
      "ScoreA": "",
      "ScoreB": "",
      "Place": "",
      "LiveLink": "",
      "Age": ""
    }
    this.matchScore = {
      "ID": "",
      "ClubA": "",
      "ClubB": "",
      "ScoreA": 0,
      "ScoreB": 0,
      "Date": "",
      "Place": "",
      "LogoA": "",
      "LogoB": ""
    }
    this.Gallery = [{

      "ID": "",
      "ImageUrl": "",
      "VideoUrl": "",
      "DescriptionA": "",
      "DescriptionE": "",
      "Date" : ""
    }] ;

    this.TableAMales = [
      {
        "ID": "",
        "ClubA": {
          "ID" : "",
          "NameA" : "",
          "Logo" : ""
        },
        "ClubB": {
          "ID" : "",
          "NameA" : "",
          "Logo" : ""
        },
        "NumberOfMatches": "",
        "Winner": "",
        "Lose": "",
        "Points": "",
        "INGoals": "",
        "OutGoals": "",
        "DivisionID": "",
        "AgeID": "",
        "Gender": "",
        "RoundID": ""
    }];

    this.TableAFemales = [
      {
        "ID": "",
        "ClubA": {
          "ID" : "",
          "NameA" : "",
          "Logo" : ""
        },
        "ClubB": {
          "ID" : "",
          "NameA" : "",
          "Logo" : ""
        },
        "NumberOfMatches": '',
        "Winner": "",
        "Lose": "",
        "Points": "",
        "INGoals": "",
        "OutGoals": "",
        "DivisionID": "",
        'AgeID': "",
        "Gender": "",
        "RoundID": ""
    }];

    this.News = [
    {
      "ID": "",
      "TitleA": "",
      "TitleE": "",
      "ShortDescriptionA": "",
      "ShortDescriptionE": "",
      "ImageUrl": "",
      "CreationDate": "",
      "ModifiedDate": "",
      "Views": "",
      "ShareLink": "",
      "Clubs": []
    }
    ,
    {
      "ID": "",
      "TitleA": "",
      "TitleE": "",
      "ShortDescriptionA": "",
      "ShortDescriptionE": "",
      "ImageUrl": "",
      "CreationDate": "",
      "ModifiedDate": "",
      "Views": "",
      "ShareLink": "",
      "Clubs": []
    }
    ,
    {
      "ID": "",
      "TitleA": "",
      "TitleE": "",
      "ShortDescriptionA": "",
      "ShortDescriptionE": "",
      "ImageUrl": "",
      "CreationDate": "",
      "ModifiedDate": "",
      "Views": "",
      "ShareLink": "",
      "Clubs": []
    }
    ,
    {
      "ID": "",
      "TitleA": "",
      "TitleE": "",
      "ShortDescriptionA": "",
      "ShortDescriptionE": "",
      "ImageUrl": "",
      "CreationDate": "",
      "ModifiedDate": "",
      "Views": "",
      "ShareLink": "",
      "Clubs": []
    }
    ,
    {
      "ID": "",
      "TitleA": "",
      "TitleE": "",
      "ShortDescriptionA": "",
      "ShortDescriptionE": "",
      "ImageUrl": "",
      "CreationDate": "",
      "ModifiedDate": "",
      "Views": "",
      "ShareLink": "",
      "Clubs": []
    }
  ]

  this.FPlayers = [
    {
      "ID": "",
      "NameA": "",
      "NameE": "",
      "ImageUrl": "",
      "Age": ""
    },
    {
      "ID": "",
      "NameA": "",
      "NameE": "",
      "ImageUrl": "",
      "Age": ""
    },{
      "ID": "",
      "NameA": "",
      "NameE": "",
      "ImageUrl": "",
      "Age": ""
    },{
      "ID": "",
      "NameA": "",
      "NameE": "",
      "ImageUrl": "",
      "Age": ""
    }
  ]
  }
  InitSlider() {
    jQuery(document).ready(function($) {

      'use strict';
        // ***************************
        // Sticky Header Function
        // ***************************
        jQuery('#importantNews').click();
        jQuery(window).scroll(function() {
            if (jQuery(this).scrollTop() > 170) {
                jQuery('body').addClass("sportsmagazine-sticky");
            }
            else {
                jQuery('body').removeClass("sportsmagazine-sticky");
            }
        });

        // ***************************
        // BannerOne Functions
        // ***************************
          jQuery('.sportsmagazine-banner-one').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              autoplay: true,
              autoplaySpeed: 2000,
              infinite: true,
              dots: false,
              arrows: false,
              fade: true,
              responsive: [
                    {
                      breakpoint: 1024,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                      }
                    },
                    {
                      breakpoint: 800,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    },
                    {
                      breakpoint: 400,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    }
                  ]
          });
        // ***************************
        // fixtureSlider Functions
        // ***************************
          jQuery('.sportsmagazine-fixture-slider').slick({
              slidesToShow: 6,
              slidesToScroll: 1,
              autoplay: true,
              autoplaySpeed: 2000,
              infinite: true,
              dots: false,
              prevArrow: "<span class='slick-arrow-left'><i class='fa fa-angle-left'></i></span>",
              nextArrow: "<span class='slick-arrow-right'><i class='fa fa-angle-right'></i></span>",
              responsive: [
                    {
                      breakpoint: 1024,
                      settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                      }
                    },
                    {
                      breakpoint: 800,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                      }
                    },
                    {
                      breakpoint: 400,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    }
                  ]
            });
        // ***************************
        // FeaturedSlider Functions
        // ***************************
          jQuery('.sportsmagazine-featured-slider').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              autoplay: true,
              autoplaySpeed: 2000,
              infinite: true,
              dots: false,
              prevArrow: "<span class='slick-arrow-left'><i class='fa fa-angle-left'></i></span>",
              nextArrow: "<span class='slick-arrow-right'><i class='fa fa-angle-right'></i></span>",
              responsive: [
                    {
                      breakpoint: 1024,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                      }
                    },
                    {
                      breakpoint: 800,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    },
                    {
                      breakpoint: 400,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    }
                  ]
            });

        // ***************************
        // ThumbSlider Functions
        // ***************************
        jQuery('.sportsmagazine-player-slider-image').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              fade: true,
              autoplay: true,
              autoplaySpeed: 2000,
              asNavFor: '.sportsmagazine-player-slider-nav'
        });

        jQuery('.sportsmagazine-player-slider-nav').slick({
              slidesToShow: 4,
              slidesToScroll: 1,
              asNavFor: '.sportsmagazine-player-slider-image',
              dots: false,
              vertical: true,
              arrows: false,
              centerMode: false,
              autoplay: true,
              autoplaySpeed: 2000,
              focusOnSelect: true,
              responsive: [
                    {
                      breakpoint: 1024,
                      settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        infinite: true,
                        vertical: true,
                      }
                    },
                    {
                      breakpoint: 800,
                      settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        vertical: true,
                      }
                    },
                    {
                      breakpoint: 400,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        vertical: true,
                      }
                    }
                  ],
        });
        // ***************************
        // BannerOne Functions
        // ***************************
          jQuery('.sportsmagazine-ticker-slide').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              autoplay: true,
              autoplaySpeed: 1000,
              infinite: true,
              dots: false,
              arrows: false,
              responsive: [
                    {
                      breakpoint: 1024,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                      }
                    },
                    {
                      breakpoint: 800,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    },
                    {
                      breakpoint: 400,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    }
                  ]
            });
        // ***************************
        // ThumbSlider Functions
        // ***************************
        jQuery('.widget-images-thumb').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              fade: true,
              asNavFor: '.widget-images-list'
            });
            jQuery('.widget-images-list').slick({
              slidesToShow: 4,
              slidesToScroll: 1,
              asNavFor: '.widget-images-thumb',
              dots: false,
              vertical: false,
              prevArrow: "<span class='slick-arrow-left'><i class='fa fa-angle-left'></i></span>",
              nextArrow: "<span class='slick-arrow-right'><i class='fa fa-angle-right'></i></span>",
              centerMode: false,
              focusOnSelect: true,
              responsive: [
                    {
                      breakpoint: 1024,
                      settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        infinite: true,
                        vertical: false,
                      }
                    },
                    {
                      breakpoint: 800,
                      settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1,
                        vertical: false,
                      }
                    },
                    {
                      breakpoint: 400,
                      settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        vertical: false,
                      }
                    }
                  ],
            });
        // ***************************
        // counter Functions
        // ***************************
          jQuery('.counter-slider').slick({
              slidesToShow: 3,
              slidesToScroll: 1,
              autoplay: true,
              autoplaySpeed: 2000,
              infinite: true,
              dots: false,
              prevArrow: "<span class='slick-arrow-left'><i class='icon-arrows-2'></i></span>",
              nextArrow: "<span class='slick-arrow-right'><i class='icon-arrows-2'></i></span>",
              responsive: [
                    {
                      breakpoint: 1024,
                      settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                      }
                    },
                    {
                      breakpoint: 800,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                      }
                    },
                    {
                      breakpoint: 400,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    }
                  ]
            });
        // ***************************
        // BannerTwo Functions
        // ***************************
          jQuery('.sportsmagazine-banner-two').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              autoplay: true,
              autoplaySpeed: 2000,
              infinite: true,
              dots: true,
              arrows: false,
              fade: true,
              responsive: [
                    {
                      breakpoint: 1024,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                      }
                    },
                    {
                      breakpoint: 800,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    },
                    {
                      breakpoint: 400,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    }
                  ]
            });
        // ***************************
        // PartnerSlider Functions
        // ***************************
          jQuery('.sportsmagazine-partner-slider').slick({
              slidesToShow: 4,
              slidesToScroll: 1,
              autoplay: true,
              autoplaySpeed: 2000,
              infinite: true,
              dots: false,
              prevArrow: "<span class='slick-arrow-left'><i class='icon-arrows-2'></i></span>",
              nextArrow: "<span class='slick-arrow-right'><i class='icon-arrows-2'></i></span>",
              responsive: [
                    {
                      breakpoint: 1024,
                      settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                      }
                    },
                    {
                      breakpoint: 800,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                      }
                    },
                    {
                      breakpoint: 400,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    }
                  ]
            });
        // ***************************
        // Widget Awards Functions
        // ***************************
          jQuery('.widget_awards').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              autoplay: true,
              autoplaySpeed: 2000,
              infinite: true,
              dots: false,
              prevArrow: "<span class='slick-arrow-left'><i class='icon-arrows-2'></i></span>",
              nextArrow: "<span class='slick-arrow-right'><i class='icon-arrows-2'></i></span>",
              responsive: [
                    {
                      breakpoint: 1024,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                      }
                    },
                    {
                      breakpoint: 800,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    },
                    {
                      breakpoint: 400,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    }
                  ]
            });

      }
    )


  }
  LastestNews() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/News/LastNews').subscribe((res: any) => {
      if (res.isSuccess) {
        this.LastNews = res.Response.LastNews;
        console.log(this.LastNews);
      }


    });
  }
  LastScoreRes() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Matches/MatcheScore').subscribe((res:any)=> {
      if (res.isSuccess) {
        if(res.Response.MatcheScore[0] != undefined) {
        this.LastScore = res.Response.MatcheScore[0];
        }
        console.log(this.LastScore);
      }

    });
  }
  GetAllNews() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/News/AllNews').subscribe((res: any) => {
    if (res.isSuccess) {
          for(let  i = 0 ; this.News != undefined && res.Response.News != undefined && i< res.Response.News.length && i< 5  ; i++) {
            this.News[i] = res.Response.News[i] ;
          }
          console.log(this.News) ;
      }
    });
  }
  GetTableAMales() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Matches/FilteredTableMatches?AgeID=4&DivisionID=4&RoundID=0').subscribe((res: any) => {
      if (res.isSuccess) {
        this.TableAMales = res.Response.TableMatches;
        console.log(this.TableAMales);
      }

    });
  }
  GetTableAFemales() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Matches/FilteredTableMatches?AgeID=5&DivisionID=1&RoundID=0').subscribe((res: any) => {
      if (res.isSuccess) {
        this.TableAFemales = res.Response.TableMatches;
        console.log(this.TableAFemales);
      }

    });
  }

  GetFamousPlayers() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Players/FamousPlayers').subscribe((res: any) => {
      if (res.isSuccess) {
        for(let  i = 0 ; this.FPlayers != undefined && res.Response.FamousPlayers != undefined && i< res.Response.FamousPlayers.length && i< 5  ; i++) {
          if(res.Response.FamousPlayers[i].PlayerType === 'InterNational') {
            this.FPlayers[i] = res.Response.FamousPlayers[i];
          }
        }
        console.log('FPlayers: ');
        console.log(this.FPlayers);
      }
    });

  }
  GetGallery() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Clubs/AllGallery').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Gallery = res.Response.Gallery ;
        console.log(this.Gallery ) ;
      }
    });
  }
  errorHandler(event) {
    console.debug(event);
    event.target.src = 'assets/images/logo.jpg';
  }

  getAllMatchScore() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Matches/MatcheScore').subscribe((res: any) => {
      if (res.isSuccess) {
        this.matchScore = res.Response.MatcheScore ;
        console.log(this.matchScore );
      }
    });
  }
  getImageHeader(){
    this.http.get('http://yakensolution.cloudapp.net/EB/api/HomePages/GetHeaderPhoto').subscribe((res: any) => {
      if (res.isSuccess) {
        this.headerImage = res.Response ;
        console.log(this.headerImage);
      }
    });
  }
  getImportantNews(){
    this.http.get('http://yakensolution.cloudapp.net/EB/api/News/ImportantNews').subscribe((res: any) => {
      if (res.isSuccess) {
        this.ImportantNews = res.Response.LastNews ;
        console.log(this.ImportantNews);
      }
    });
  }
  compare(data){
    console.log(data.Email);
    this.http.post('http://yakensolution.cloudapp.net/EB/api/UserNotifications/Subscribe', data).subscribe((res: any) => {
      console.log(res);
      if (res.isSuccess) {
        alert('Success Subscribe');
      } else {
        alert(res.errorMessage);
      }
    });
  }
  ngOnInit() {
    this.InitSlider() ;
    this.LastestNews() ;
    this.LastScoreRes();
    this.GetAllNews() ;
    this.GetTableAMales() ;
    this.GetTableAFemales() ;
    this.GetFamousPlayers() ;
    this.GetGallery() ;
    this.getAllMatchScore();
    this.getImageHeader();
    this.getImportantNews();
  }

}
