import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.css']
})
export class ArchiveComponent implements OnInit {

  ArchiveCategories : any ;

  constructor(public http: HttpClient, public Route: ActivatedRoute) {
    this.ArchiveCategories = [{
      "ID": "",
      "NameA": "",
      "NameE": "",
    }] ;
  }

  GetArchiveCategories() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Archive/ArchiveCategories').subscribe((res: any) => {
      if (res.isSuccess) {
        this.ArchiveCategories = res.Response.ArchiveCategories;
        console.log(this.ArchiveCategories);
      }

    });
  }

  ngOnInit() {
    this.GetArchiveCategories();
  }

}
