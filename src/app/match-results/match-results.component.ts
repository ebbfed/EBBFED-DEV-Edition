import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-match-results',
  templateUrl: './match-results.component.html',
  styleUrls: ['./match-results.component.css']
})
export class MatchResultsComponent implements OnInit {

  LastScore: any;
  LastScoreN: any;
  FilteredMatches: any;
  Age: any; Div: any; Round: any;
  Ages: any; Divs: any; Rounds: any;
  Clubs: any;
  ClubName: String;

  constructor(private http: HttpClient, public Route: Router) {
    this.LastScore = [{
      "ID": "",
      "ClubA": "",
      "ClubB": "",
      "Division": {
        "ID": "",
        "NameA": "",
        "NameE": ""
      },
      "Round": {
        "ID": "",
        "NameA": "",
        "NameE": ""
      },
      "Age": {
        "ID": "",
        "NameA": "",
        "NameE": ""
      },
      "Gender": "",
      "Date": "",
      "ScoreA": "",
      "ScoreB": "",
      "Place": "",
      "LiveLink": "",
    }];

    this.LastScoreN = [{
      "ID": "",
      "NationalA": "",
      "NationalB": "",
      "Place": "",
      "ScoreA": "",
      "ScoreB": "",
      "Date": ""
    }];

    this.FilteredMatches = [];
    this.Clubs = [{
      "ID": "",
      "NameA": "",
      "NameE": "",
      "DescriptionA": "",
      "DescriptionE": "",
      "EstablishedYear": "",
      "NumberTitles": "",
      "Division": {
        "ID": "",
        "NameA": "",
        "NameE": ""
      },
      "Round": {
        "ID": "",
        "NameA": "",
        "NameE": ""
      },
      "Age": {
        "ID": "",
        "NameA": "",
        "NameE": ""
      },
      "Logo": ""
    }];
    this.Ages = [{
      "ID": "",
      "NameA": "",
      "NameE": ""
    }];
    this.Divs = [{
      "ID": "",
      "NameA": "",
      "NameE": ""
    }];
    this.Rounds = [{
      "ID": "",
      "NameA": "",
      "NameE": ""
    }];
  }

  LastScoreRes() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Matches/MatcheScore').subscribe((res: any) => {
      if (res.isSuccess) {
        this.LastScore = res.Response.MatcheScore;
        this.FilteredMatches = this.LastScore;
        console.log(this.LastScore);
      }

    });
  }
  LastNScoreRes() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/NationalMatches/MatcheScore').subscribe((res: any) => {
      if (res.isSuccess) {
        this.LastScoreN = res.Response.MatcheScore;
        this.FilteredMatches = this.LastScoreN;
        console.log(this.LastScoreN);
      }

    });
  }

  GetAllClubs() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Clubs/AllClubs').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Clubs = res.Response.Players;
        console.log(this.Clubs);
      }
    });

  }

  GetAllNationalTeams() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/NationalMatches/AllNationalTeams').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Clubs = res.Response.AllNationalTeams;
        console.log(this.Clubs);
      }
    });

  }

  GetAges() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Settings/AllAges').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Ages = res.Response.Ages;
        console.log(this.Ages);
      }

    });
  }
  GetDivs() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Settings/AllAges').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Divs = res.Response.Ages;
        console.log(this.Divs);
      }
    });
  }
  GetRounds() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Rounds/AllRounds').subscribe((res: any) => {
      if (res.isSuccess) {
        this.Rounds = res.Response.Rounds;
        console.log(this.Rounds);
      }
    });
  }
  FilterMatches(x, y, z) {
    this.FilteredMatches = [];
    console.log(x);
    console.log(y);
    console.log(z);

    if ((x == undefined || x == '') && (y == undefined || y == '') && (z == '' || z == undefined)) {
      this.FilteredMatches = this.LastScore;
    }
    else if ((x != undefined && x != '') && (y == undefined || y == '') && (z == '' || z == undefined)) {
      for (var i = 0; this.LastScore != undefined && i < this.LastScore.length; i++) {
        if (this.LastScore[i].AgeID == x) {
          this.FilteredMatches.push(this.LastScore[i]);
        }
      }

    }
    else if ((x == undefined || x == '') && (y != undefined || y != '') && (z == '' || z == undefined)) {
      for (var i = 0; this.LastScore != undefined && i < this.LastScore.length; i++) {
        if (this.LastScore[i].DivisionID === y)
          this.FilteredMatches.push(this.LastScore[i]);
      }

    }
    else if ((x == undefined || x == '') && (y == undefined || y == '') && (z != '' || z != undefined)) {
      for (var i = 0; this.LastScore != undefined && i < this.LastScore.length; i++) {
        if (this.LastScore[i].ClubA === z || this.LastScore[i].ClubB === z)
          this.FilteredMatches.push(this.LastScore[i]);
      }

    }
    else if ((x != undefined || x != '') && (y != undefined || y != '') && (z == '' || z == undefined)) {
      for (var i = 0; this.LastScore != undefined && i < this.LastScore.length; i++) {
        if (this.LastScore[i].DivisionID === y && this.LastScore[i].AgeID === x)
          this.FilteredMatches.push(this.LastScore[i]);
      }
    }
    else if ((x != undefined || x != '') && (y == undefined || y == '') && (z != '' || z != undefined)) {
      for (var i = 0; this.LastScore != undefined && i < this.LastScore.length; i++) {
        if (this.LastScore[i].ClubA === z || this.LastScore[i].ClubB === z && this.LastScore[i].AgeID === x)
          this.FilteredMatches.push(this.LastScore[i]);
      }
    }
    else if ((x == undefined || x == '') && (y != undefined || y != '') && (z != '' || z != undefined)) {
      for (var i = 0; this.LastScore != undefined && i < this.LastScore.length; i++) {
        if (this.LastScore[i].ClubA === z || this.LastScore[i].ClubB === z && this.LastScore[i].DivisionID === y)
          this.FilteredMatches.push(this.LastScore[i]);
      }
    }
    else {
      for (var i = 0; this.LastScore != undefined && i < this.LastScore.length; i++) {
        if (this.LastScore[i].ClubA === z || this.LastScore[i].ClubB === z && this.LastScore[i].DivisionID === y && this.LastScore[i].AgeID === x)
          this.FilteredMatches.push(this.LastScore[i]);
      }
    }
    console.log(this.FilteredMatches);

  }
  FilterNMatches(x, y) {
    this.FilteredMatches = [];
    console.log(x);
    console.log(y);
    if ((x == undefined || x == '') && (y == undefined || y == '')) {
      this.FilteredMatches = this.LastScore;
    }
    else if ((x != undefined && x != '') && (y == undefined || y == '')) {
      for (var i = 0; this.LastScore != undefined && i < this.LastScore.length; i++) {
        if (this.LastScore[i].AgeID == x) {
          this.FilteredMatches.push(this.LastScore[i]);
        }
      }

    }
    else if ((x == undefined || x == '') && (y != undefined || y != '')) {
      for (var i = 0; this.LastScore != undefined && i < this.LastScore.length; i++) {
        if (this.LastScore[i].DivisionID === y)
          this.FilteredMatches.push(this.LastScore[i]);
      }

    }
    else {
      for (var i = 0; this.LastScore != undefined && i < this.LastScore.length; i++) {
        if (this.LastScore[i].DivisionID === y && this.LastScore[i].AgeID === x)
          this.FilteredMatches.push(this.LastScore[i]);
      }
    }
    console.log(this.FilteredMatches);

  }

  FilterNMatchesByRound(x) {
    this.FilteredMatches = [];

    if ((x == undefined || x == '')) {
      this.FilteredMatches = this.LastScore;
    }
    else if ((x != undefined && x != '')) {
      for (var i = 0; this.LastScore != undefined && i < this.LastScore.length; i++) {
        if (this.LastScore[i].Round != null) {
          if (this.LastScore[i].Round.ID == x) {
            this.FilteredMatches.push(this.LastScore[i]);
          }
        }
      }
    }
    console.log(this.FilteredMatches);
  }

  FilterNMatchesByRoundAndDiv(x, y) {
    this.FilteredMatches = [];
    console.log(x);
    console.log(y);

    if ((x == undefined || x == '') && (y == undefined || y == '')) {
      this.FilteredMatches = this.LastScore;
    }
    else if ((x != undefined && x != '') && (y == undefined || y == '')) {
      for (var i = 0; this.LastScore != undefined && i < this.LastScore.length; i++) {
        if (this.LastScore[i].Round.ID == x) {
          this.FilteredMatches.push(this.LastScore[i]);
        }
      }

    }
    else if ((x == undefined || x == '') && (y != undefined || y != '')) {
      for (var i = 0; this.LastScore != undefined && i < this.LastScore.length; i++) {
        if (this.LastScore[i].Division.ID === y)
          this.FilteredMatches.push(this.LastScore[i]);
      }

    }
    else {
      for (var i = 0; this.LastScore != undefined && i < this.LastScore.length; i++) {
        if (this.LastScore[i].Division.ID === y && this.LastScore[i].Round.ID === x)
          this.FilteredMatches.push(this.LastScore[i]);
      }
    }
    console.log(this.FilteredMatches);
  }

  FilterNMatchesByClub(x) {
    this.FilteredMatches = [];

    if ((x == undefined || x == '')) {
      this.FilteredMatches = this.LastScoreN;
    }
    else if ((x != undefined && x != '')) {
      for (var i = 0; this.LastScoreN != undefined && i < this.LastScoreN.length; i++) {
        if (this.LastScoreN[i].ClubA == x) {
          this.FilteredMatches.push(this.LastScoreN[i]);
        }
      }
    }
    console.log(this.LastScoreN);
    console.log(this.FilteredMatches);
  }

  errorHandler(event) {
    console.debug(event);
    event.target.src = "assets/images/logo.jpg";
  }

  ngOnInit() {
    if (this.Route.url === '/match-result') {
      this.LastScoreRes();
      this.GetAllClubs();
      this.GetAges();
      this.GetDivs();
      this.GetRounds();
    }
    else {
      this.LastNScoreRes();
      this.GetAges();
      this.GetDivs();
      this.GetRounds();
      this.GetAllNationalTeams();
    }

  }


  Filters(data){
    let filter='';
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        if(key==='gender' && data[key]!==''){
          if(filter === ''){
            filter+= ('gender=' + data[key])
          } else {
            filter+= ('&gender=' + data[key])
          }
        }
        if(key==='age' && data[key]!==''){
          if(filter === ''){
            filter+= ('age=' + data[key])
          } else {
            filter+= ('&age=' + data[key])
          }
        }

      }
    }
    console.log(filter);
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Matches/FilteredMatcheScore?' + filter).subscribe((res:any)=>{
      this.FilteredMatches = res.Response.MatcheScore;
      console.log(res.Response);
    })
    // this.http.get()
  }

}
