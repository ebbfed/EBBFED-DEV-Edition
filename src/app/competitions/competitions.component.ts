import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-competitions',
  templateUrl: './competitions.component.html',
  styleUrls: ['./competitions.component.css']
})
export class CompetitionsComponent implements OnInit {

  AllComps : any ;

  constructor(public http: HttpClient, public Route: ActivatedRoute) {
    this.AllComps = [{
      "ID": "",
      "NameA": "",
      "NameE": "",
      "Date": "",
      "Location": "",
      "RegisterLink": ""
    }] ;
   }

   GetAllComps() {
    this.http.get('http://yakensolution.cloudapp.net/EB/api/Competitions/AllCompetitions').subscribe((res: any) => {
      if (res.isSuccess) {
        this.AllComps = res.Response.AllCompetitions;
        console.log(this.AllComps);
      }

    });
  }

  ngOnInit() {
    this.GetAllComps();
  }

}
